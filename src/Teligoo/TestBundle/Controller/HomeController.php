<?php

namespace Teligoo\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Teligoo\TestBundle\Entity\Users;
use Teligoo\TestBundle\Form\UsersType;
use Zend\Soap;



class HomeController extends Controller
{
    public function indexAction(Request $request)
    {

        $session =  new Session();
        $login = $session->get('login', '');

        $userEntity  = new Users();
        $newUserForm = $this->createForm(new UsersType(), $userEntity);
        $newUserForm->bind($request);

        if ($newUserForm->isValid()) {
            $client = new Soap\Client($this->generateUrl('teligoo_test_soap', array(), true)."?wsdl");

            $client->createUser(
                $userEntity->getUsername(),
                $userEntity->getPassword(),
                $userEntity->getFirstname(),
                $userEntity->getLastname()
            );

            return $this->redirect($this->generateUrl('teligoo_test_home', array(), true));
        }

        $loginForm = $this->createFormBuilder(array())
            ->add('Login', 'text')
            ->add('Password', 'password')
            ->getForm();

        return $this->render('TeligooTestBundle:Home:index.html.twig',array(
                'newUserForm'   => $newUserForm->createView(),
                'loginForm'     => $loginForm->createView(),
                'login'         => $login

            )
        );
    }

    public function loginAction(Request $request){
        $form = $request->request->get('form');

        $client = new Soap\Client($this->generateUrl('teligoo_test_soap', array(), true)."?wsdl");

        $response = $client->validateUser(
                $form['Login'],
                $form['Password']
        );

        if($response){
            $session = new Session();
            $session->set('login', $response);

        }

        return $this->redirect($this->generateUrl('teligoo_test_home', array(), true));
    }
}
