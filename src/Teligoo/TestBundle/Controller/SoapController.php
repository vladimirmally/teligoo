<?php

namespace Teligoo\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Zend\Soap;


class SoapController extends Controller
{
    public function indexAction()
    {

        if(isset($_GET['wsdl'])) {
            return $this->handleWSDL($this->generateUrl('teligoo_test_soap', array(), true), 'api_service');
        } else {
            return $this->handleSOAP($this->generateUrl('teligoo_test_soap', array(), true), 'api_service');
        }

    }

    public function handleWSDL($uri, $class)
    {
        $autodiscover = new Soap\AutoDiscover();


        $autodiscover->setClass($this->get($class));
        $autodiscover->setUri($uri);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=UTF-8');

        ob_start();
        $autodiscover->handle();
        $response->setContent(ob_get_clean());
        return $response;
    }

    public function handleSOAP($uri, $class)
    {
        $soap = new Soap\Server(null,
            array(
                'location' => $uri,
                'uri' => $uri
            ));


        $soap->setClass($this->get($class));
        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=UTF-8');

        ob_start();
        $soap->handle();


        $response->setContent(ob_get_clean());
        return $response;
    }
}
