<?php

namespace Teligoo\TestBundle\Services;

use Doctrine\ORM\EntityManager;

class ApiService
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * This method add new user
     *
     * @param string $userName
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @return string
     */

    function createUser($userName, $password, $firstName, $lastName){
        $repository = $this->em->getRepository('TeligooTestBundle:Users');
        $repository->addUser($userName, $password, $firstName, $lastName);
        return true;
    }


     /**
      * This method check login/password for validate user
      *
      * @param string $userName
      * @param string $password
      * @return string
      */
    function validateUser($userName, $password){

        $repository = $this->em->getRepository('TeligooTestBundle:Users');
        $result = $repository->getUser($userName, $password);

        if(empty($result)){
            return false;
        }else{
            return $result[0]->getUsername();
        }

    }

}