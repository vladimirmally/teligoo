<?php

namespace Teligoo\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 */
class Users
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $Username;

    /**
     * @var string
     */
    private $Password;

    /**
     * @var string
     */
    private $Firstname;

    /**
     * @var string
     */
    private $Lastname;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Username
     *
     * @param string $username
     * @return Users
     */
    public function setUsername($username)
    {
        $this->Username = $username;

        return $this;
    }

    /**
     * Get Username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * Set Password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $this->Password = $password;

        return $this;
    }

    /**
     * Get Password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * Set Firstname
     *
     * @param string $firstname
     * @return Users
     */
    public function setFirstname($firstname)
    {
        $this->Firstname = $firstname;

        return $this;
    }

    /**
     * Get Firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->Firstname;
    }

    /**
     * Set Lastname
     *
     * @param string $lastname
     * @return Users
     */
    public function setLastname($lastname)
    {
        $this->Lastname = $lastname;

        return $this;
    }

    /**
     * Get Lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->Lastname;
    }
}
